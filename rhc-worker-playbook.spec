%bcond_without check

%global goipath     github.com/redhatinsights/rhc-worker-playbook
Version:            0.2.3
%global tag         v%{version}

%gometa -f

%global common_description %{expand:
A worker for yggdrasil that receives Ansible playbooks and executes them against
the local host.}

%global golicenses  LICENSE
%global godocs      CONTRIBUTING.md README.md

Name:       rhc-worker-playbook
Release:    %autorelease
Summary:    Ansible playbook yggdrasil worker

License:    GPL-2.0-or-later
URL:        %{gourl}
Source:     %{url}/releases/download/%{tag}/rhc-worker-playbook-%{version}.tar.xz

BuildRequires:      ansible-core
BuildRequires:      golang >= 1.18
BuildRequires:      jq
BuildRequires:      meson
BuildRequires:      pkgconfig(dbus-1)
BuildRequires:      pkgconfig(systemd)
BuildRequires:      pkgconfig(yggdrasil)
BuildRequires:      python3dist(pip)
BuildRequires:      systemd-rpm-macros
BuildRequires:      python3-rpm-macros

%if %{with check}
BuildRequires:      insights-client
%endif

Requires:           ansible-core

%description %common_description

%global setup_flags -Dvendor=True

%gopkg

%prep
%goprep %{?rhel:-k}

%if %{undefined rhel}
%generate_buildrequires
%go_generate_buildrequires
%endif

%build
%undefine _auto_set_build_flags
export %gomodulesmode
%{?gobuilddir:export GOPATH="%{gobuilddir}:${GOPATH:+${GOPATH}:}%{?gopath}"}
%meson %setup_flags "-Dgobuildflags=[%(echo %{expand:%gocompilerflags} | sed -e s/"^"/"'"/ -e s/" "/"', '"/g -e s/"$"/"'"/), '-tags', '"rpm_crashtraceback\ ${BUILDTAGS:-}"', '-a', '-v', '-x']" -Dgoldflags='%{?currentgoldflags} -B 0x%(head -c20 /dev/urandom|od -An -tx1|tr -d " \n") -compressdwarf=false -linkmode=external -extldflags "%{build_ldflags} %{?__golang_extldflags}"'
%meson_build

%install
%meson_install
%py3_shebang_fix \
    %{buildroot}%{_datadir}/rhc-worker-playbook/ansible/collections/ansible_collections/ansible/posix/.azure-pipelines/scripts/*.py \
    %{buildroot}%{_datadir}/rhc-worker-playbook/ansible/collections/ansible_collections/ansible/posix/tests/utils/shippable/*.py \
    %{buildroot}%{_datadir}/rhc-worker-playbook/ansible/collections/ansible_collections/community/general/.azure-pipelines/scripts/*.py \
    %{buildroot}%{_datadir}/rhc-worker-playbook/ansible/collections/ansible_collections/community/general/tests/*/extra/*.py \
    %{buildroot}%{_datadir}/rhc-worker-playbook/ansible/collections/ansible_collections/community/general/tests/integration/targets/django_manage/files/base_test/1045-single-app-project/single_app_project/*.py \
    %{buildroot}%{_datadir}/rhc-worker-playbook/ansible/collections/ansible_collections/community/general/tests/integration/targets/django_manage/files/base_test/simple_project/p1/*.py \
    %{nil}
%{__install} --directory %{buildroot}%{_localstatedir}/lib/%{name}

%if %{with check}
%check
%meson_test
%endif

%post
%systemd_post com.redhat.Yggdrasil1.Worker1.rhc_worker_playbook.service

%preun
%systemd_preun com.redhat.Yggdrasil1.Worker1.rhc_worker_playbook.service

%postun
%systemd_postun_with_restart com.redhat.Yggdrasil1.Worker1.rhc_worker_playbook.service

%files
%license LICENSE
%if %{defined rhel}
%license vendor/modules.txt
%endif
%doc CONTRIBUTING.md README.md
%{_libexecdir}/*
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.toml
%{_unitdir}/*
%{_datadir}/dbus-1/{interfaces,system-services,system.d}/*
%{_datadir}/%{name}
%{_libdir}/%{name}
%dir %attr(700, root, yggdrasil-worker) %{_localstatedir}/lib/%{name}

%changelog
%autochangelog
